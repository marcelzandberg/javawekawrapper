# Wekka Wrapper ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

Wekka Wrapper is a java api for classifing unknown instances by using weka.


### Requirements
* Linux
* Java 8 and up
* Weka 3 and up
* Intellij


## Files Includes With This Project:
* Project folder: nl.bioinf.mazandberg.readmitted
* samplefile: sampleData.arff
* modelfile: RandomForest.model
* unknownfile: unknownFile.arff


## Usage
* Start Intellij and open up the project folder
* Command Line Arguments:
* Example: -d theData/sampleData.arff -m theData/RandomForest.model -o theData/unknownFile.arff
* Required:

usage: Wekka wrapper
 -d,--data <arg>     Data file path for this session;
 -m,--model <arg>    User model path for the data
 -o,--output <arg>   file path with unknown instances

argument -d: done with value: theData/sampleData.arff

argument -m: done with value: theData/RandomForest.model

argument -o: done with value: theData/unknownFile.arff

* hit the run button and voila


## Running the tests
* The Weka Wrapper uses weka which trains de sampleData with the modelFile to classify the unknown instances.
*  The results of the newly classified instances is written in the file 'results.arff' which can be found in the project folder under results.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

Note! all the commit logs for this repo can be found in my other repo called themaopdracht_thema09

## License
[MIT](https://choosealicense.com/licenses/mit/)