package nl.bioinf.arguments_provider_demo;


//imports
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import java.io.File;
import java.io.IOException;

/**
The WekaRusults program is an api for weka.
*It implemts the ApachiCliOptionsProvider and uses its arguments
*It uses the core elements of weka
*It gets a data file, model file and an file with unknown instances.
*It returns the predicted values for the unknown instances
*
*@author mazandberg
*@version 1.0
*@since 12/12/2018
*/
public class WekaResults {
    /**
     *Gets the options from the optionsprovider
     */
    private final OptionsProvider optionsProvider;
    //has the options from the cmd
    public WekaResults(final OptionsProvider optionsProvider) {
        this.optionsProvider = optionsProvider;
    }
    
    /**
     *starting the program
     *uses the data file and the unknown instance file
     *@exception Exception on input error
     */
    public void start() {

        try {
            String datafile = optionsProvider.getDatafile();

            String unknownFile = optionsProvider.getOutfile();
            Instances instances = loadArff(datafile);
            printInstances(instances);
            RandomForest randomForest = buildClassifier(instances);
            saveClassifier(randomForest);
            RandomForest fromFile = loadClassifier();
            Instances unknownInstances = loadArff(unknownFile);

            classifyNewInstance(fromFile, unknownInstances);

        } catch (Exception e) {
            //e.printStackTrace();
            //System.err.println();
        }
    }
    
    
    /**
     *Instances will be classified here and saved to:
     *'results/results.arff'
     *@param tree, the machine learning algoritm that is used to classify the instances
     *@param unknownInstances, file with the unknown instances
     */
    private void classifyNewInstance(RandomForest tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        //writes the results to an arff file
        ArffSaver saver = new ArffSaver();
        saver.setInstances(labeled);
        saver.setFile(new File("./results/results.arff"));
        saver.writeBatch();
        System.out.println("\nLabeled data is appended to file: 'results/results.arff'");

    }


    private RandomForest loadClassifier() throws Exception {
        // deserialize model
        String modelFile = optionsProvider.getModelfile();
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);
    }

    private void saveClassifier(RandomForest randomForest) throws Exception {
        //post 3.5.5
        // serialize model
        String modelFile = optionsProvider.getModelfile();
        weka.core.SerializationHelper.write(modelFile, randomForest);

    }
    
    /**
     *The machinelearning algoritm RandomForest is build here.
     *You can set the options for the algoritm and it wil then be created
     *@param unknownInstances, file with the unknown instances
     *@return tree, the builded algoritm
     */
    private RandomForest buildClassifier(Instances instances) throws Exception {
        
        RandomForest tree = new RandomForest();         // new instance of tree
        System.out.println("\nstarting the good work, this will take a moment :)\n");
        //tree options
        tree.setMaxDepth(0);
        tree.setBreakTiesRandomly(false);
        tree.setSeed(1);
        tree.setBagSizePercent(100);
        tree.setBatchSize("100");
        tree.setCalcOutOfBag(false);
        tree.setDebug(false);
        tree.setDoNotCheckCapabilities(false);
        tree.setNumDecimalPlaces(2);
        tree.setNumExecutionSlots(1);
        tree.setNumFeatures(0);
        tree.setNumIterations(100);
        tree.setOutputOutOfBagComplexityStatistics(false);
        tree.setPrintClassifiers(false);
        tree.setStoreOutOfBagPredictions(false);
        tree.buildClassifier(instances);   // build classifier
        return tree;
    }

    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());

    }
    
    /**
     *Instances will be loaded here
     *@exception IOException could not read from file
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}
