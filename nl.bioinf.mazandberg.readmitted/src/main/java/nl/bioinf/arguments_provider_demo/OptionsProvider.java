package nl.bioinf.arguments_provider_demo;

/**
 * interface that specifies which options should be provided to the tool.
 * @author mazandberg
 */
public interface OptionsProvider {
    /**
     * serves the path of the datafile.
     * @return userName the user name
     */
    String getDatafile();
    /**
     * serves the Modelfile to be used.
     * @return Modelfile
     */
    String getModelfile();
    /**
     * serves the unknowfile to be prossed
     * @return unknowfile
     */
    String getOutfile();
}
