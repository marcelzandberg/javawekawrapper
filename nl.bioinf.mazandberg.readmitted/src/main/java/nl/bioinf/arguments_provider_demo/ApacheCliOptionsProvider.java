package nl.bioinf.arguments_provider_demo;


//imports
import java.io.File;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
*This program provides the options for the WekaRunner program.
*It can build options, checks them and then te command line is processed.
*
*@author mazandberg
*@version 1.0
*@since 12/12/2018
*/

public class ApacheCliOptionsProvider implements OptionsProvider {
    /**
    *This method is used to store and serve commandline options
    *variables for the cmd options
    *@param DATA first required option 
    *@param MODEL second required option
    *@param OUTPUT third required option
    */
    
    private static final String DATA = "data";
    private static final String MODEL = "model";
    private static final String OUTPUT = "output";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;



    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * make cmd options and help usage
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option dataOption = new Option("d", DATA, true, "Data file path for this session;");
        Option modelOption = new Option("m", MODEL, true, "User model path for the data");
        Option outOption = new Option("o", OUTPUT, true, "file path with unknown instances");
        options.addOption(dataOption);
        options.addOption(modelOption);
        options.addOption(outOption);

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Wekka wrapper", options);
    }

     /**
     * Parsing Stage.
     *creates an argparser and parses the commandline.
     *returns nothing
     *@exception ParseException On input error.
     */
    private void processCommandLine() {
       
        try {
            CommandLineParser parser = new DefaultParser();


            //parse the options passed as command line arguments

            this.commandLine = parser.parse(this.options, this.clArguments);
            //***Interrogation Stage***
            //hasOptions checks if option is present or not
            if (clArguments.length < 3) {
                System.out.println("Look at the usage! give me arguments:\n-d: \n-m: \n-o:");
                System.exit(1);
            }


            //checks if the given argument is good argument
            if (commandLine.hasOption("d")) {
                File dFile = new File(commandLine.getOptionValue("d"));
                if(dFile.exists() && dFile.isFile()) {
                    System.out.println("\nargument -d: done with value: " + commandLine.getOptionValue("d"));
                }
                else{
                    System.out.println("\nLook at the usage! give me the right file path:\n-d: c:...");
                    System.exit(1);
                }
            }
            if (commandLine.hasOption("m")) {
                File mFile = new File(commandLine.getOptionValue("m"));
                if (mFile.exists() && mFile.isFile()) {
                    System.out.println("\nargument -m: done with value: " + commandLine.getOptionValue("m"));

            } else {
                    System.out.println("\nLook at the usage! give me the right file path:\n-m: c:...");
                    System.exit(1);
                   }
            }
            if (commandLine.hasOption("o")) {
                File oFile = new File(commandLine.getOptionValue("o"));
                if (oFile.exists() && oFile.isFile()) {
                    System.out.println("\nargument -o: done with value: " + commandLine.getOptionValue("o"));
                } else {
                    System.out.println("\nLook at the usage! give me the right file path:\n-o: c:...");
                    System.exit(1);
                }
            }
            }catch (ParseException ex) {
            //throw new IllegalArgumentException(ex);
            System.err.println(ex.getLocalizedMessage());
        }
        }



    @Override
    public String getDatafile() {
        return this.commandLine.getOptionValue(DATA);
    }

    @Override
    public String getModelfile() {
        return this.commandLine.getOptionValue(MODEL);
    }

    @Override
    public String getOutfile() {
        return this.commandLine.getOptionValue(OUTPUT);
    }
}


